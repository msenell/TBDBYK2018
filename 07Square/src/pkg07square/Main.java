
package pkg07square;

import java.util.Scanner;

/*
    Kullanıcıdan sayı al.
    Bu sayının karesini ekrana yaz.
    Kullanıcı o girene kadar sayı almaya devam et.
*/
public class Main 
{

    public static void main(String[] args) 
    {
        int sayi;
        Scanner scn = new Scanner(System.in);
        /*System.out.println("Karesi alınacak sayıyı giriniz(0:Exit):");
        Scanner scn = new Scanner(System.in);
        sayi = scn.nextInt();
        while(sayi != 0)
        {
            System.out.println(sayi + "*" + sayi + "=" + sayi*sayi);
            System.out.println("Karesi alınacak sayıyı giriniz(0:Exit):");
            sayi = scn.nextInt();
        }*/
        
        do
        {
            System.out.println("Karesi alınacak sayıyı giriniz(0:Exit):");
            sayi = scn.nextInt();
            System.out.println(sayi + "*" + sayi + "=" + sayi*sayi);
        }while(sayi != 0);
    }
    
}
