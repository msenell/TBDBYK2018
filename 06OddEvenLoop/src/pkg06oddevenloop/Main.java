
package pkg06oddevenloop;

/*
    0-20 arasındaki sayıların tek olanlarını ekrana yazdıran program.
*/
public class Main 
{

    public static void main(String[] args) 
    {
        System.out.println("0 ile 20 arasındaki tek sayılar:");
        for(int i = 0; i<20; i++)
        {
            if( i%2 == 1 )
            {
                System.out.print(i + "\t");
            }
        }
    }
    
}
