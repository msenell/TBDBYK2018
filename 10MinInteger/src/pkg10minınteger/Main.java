package pkg10minınteger;

import javax.swing.JOptionPane;

public class Main 
{
    /*
        1-Kullanıcıdan OptionPane ile sayıları al.
        2-Sayıları ayır.
        3-İçlerindeki en küçük sayıyı göster.
    */

    public static void main(String[] args) 
    {
        String sayilar = JOptionPane.showInputDialog("Sayıları giriniz:");
        System.out.println("Girilen sayılar : " + sayilar);
        String sayidizisi[];
        sayidizisi = sayilar.split("-");
        int adet = sayidizisi.length;
        int[] intSayiDizisi;
        intSayiDizisi = new int[adet];
        int i = 0;
        for(String sayi : sayidizisi)
        {
            intSayiDizisi[i] = Integer.parseInt(sayi);
            i++;
        }
        System.out.println("Tamsayılar : ");
        for(i = 0; i < intSayiDizisi.length; i++ )
            System.out.println(intSayiDizisi[i]);
        
        int min = intSayiDizisi[0];
        
        for(i = 1; i < intSayiDizisi.length; i++)
        {
            if(intSayiDizisi[i] < min)
                min = intSayiDizisi[i];
        }
        
        System.out.println("Girilen sayıların en küçüğü : " + min);
    }
    
}
