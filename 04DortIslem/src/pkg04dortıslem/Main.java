package pkg04dortıslem;

import java.util.Scanner;

/*
    Kullanıcıdan iki adet sayı alınacak.
    Bu sayılarla 4 işlem yapılarak her sonuç bir değişkene atanacak.
    Daha sonra sonuçlar ekrana yazdırılacak.
*/
public class Main {

    public static void main(String[] args) 
    {
        int sayi1, sayi2;
        Scanner scn = new Scanner(System.in);
        System.out.println("Birinci sayıyı giriniz:");
        sayi1 = scn.nextInt();
        System.out.println("İkinci sayıyı giriniz:");
        sayi2 = scn.nextInt();
        
        int toplam = sayi1 + sayi2;
        
        int kalan = sayi1 - sayi2;
        
        int carpim = sayi1 * sayi2;
        
        float bolum = (float)sayi1 / sayi2;
        
        System.out.println("Sonuçlar");
        System.out.println(sayi1 + "+" + sayi2 + "=" + toplam);
        System.out.println(sayi1 + "-" + sayi2 + "=" + kalan);
        System.out.println(sayi1 + "*" + sayi2 + "=" + carpim);
        System.out.println(sayi1 + "/" + sayi2 + "=" + bolum);
    }
    
}
