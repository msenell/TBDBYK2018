/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg13selectionsort;

import java.util.Scanner;


public class Main 
{
    /*
        1- Kullanıcıdan string halinde bir sayı dizisi al.
        2- Bu stringi (,)'e göre parçala ve diziye at.
        3- Diziyi int[]'e çevir.
        4- Çevrilen diziyi selection sort algoritması ile sırala.
        5- Sıralanmış diziyi tekrar eski haline getir, ekrana yaz.
    */

    public static void main(String[] args) 
    {
        String sayilar;
        Scanner scn = new Scanner(System.in);
        System.out.println("Sıralanacak sayıları aralarına boşluk(' ') koyarak giriniz:");
        sayilar = scn.nextLine();
        String[] stSayiDizisi = sayilar.split(" ");
        int[] intSayiDizisi = new int[stSayiDizisi.length];
        System.out.println();
        
        for(int i = 0; i<stSayiDizisi.length; i++)
        {
            intSayiDizisi[i] = Integer.parseInt( stSayiDizisi[i] );
        }
        
        for(int i = 0; i < intSayiDizisi.length; i++)
        {
            for(int j = i+1; j < intSayiDizisi.length; j++ )
            {
                if(intSayiDizisi[j] < intSayiDizisi[i])
                {
                    int temp = intSayiDizisi[i];
                    intSayiDizisi[i] = intSayiDizisi[j];
                    intSayiDizisi[j] = temp;
                }
            }
            //System.out.print(intSayiDizisi[i] + ",");
            
        
        }
    }
    
}
