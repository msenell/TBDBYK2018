
package pkg08loopconbreak;

import java.util.Scanner;


public class Main 
{

    /*
        1-Kullanıcıdan bir sayı al
        2-Sayı 0 ise döngüden çık.
        3-Sayı 1 ise sonraki iterasyona geç.
        4-Diğer durumlarda sayının karesini ekrana yaz.
    */
    public static void main(String[] args)
    {
        Scanner scn = new Scanner(System.in);
        int sayi;
        
        while(true)
        {
            System.out.println("Karesi alınacak sayıyı giriniz(0: exit, 1:next):");
            sayi = scn.nextInt();
            if(sayi == 0)
                break;
            else if(sayi == 1)
                continue;
            System.out.println(sayi + "*" + sayi + "=" + sayi*sayi);
        }
    }
    
}
