
package pkg09asalsayilar;

import javax.swing.JOptionPane;

/*
    1-Kullanıcıdan bir sayı al.
    2-Girilen sayıya kadar bütün asal sayıları bulup ekrana yazdır.
*/
public class Main 
{


    public static void main(String[] args) 
    {
        String asalsayilar = "";
        int sinir
         = Integer.parseInt( JOptionPane.showInputDialog(null, "Sayı Giriniz:") );

        boolean asalMi = true;
        
        for(int j = 2; j < sinir; j++)
        {
            
            for(int i = 2; i<j; i++)
            {
                asalMi = true;
                if(j % i == 0)
                {
                    asalMi = false;
                    break;
                }     
            }
            if(asalMi)
                asalsayilar =asalsayilar + j + " ";
            
           
        }
        JOptionPane.showMessageDialog(null, asalsayilar);
    }
    
}
