package pkg02primitivetypes;

//Primitive(İlkel) Veri Tipleri

/*
    TBD Bilişim Yaz Kampı
    2018
    Java Kursu
*/

public class Main {


    public static void main(String[] args) 
    {
        byte _byte;
        _byte = 127;
        
        short _short = 10000;
        
        int _int = 100000;
        
        long _long = 999999999;
        
        char _char = 'A';
        
        _char = (char)(_char + 1);
        
        _int = (int)_char;
        
        boolean _boolean = false;
        
        float _float = 1.4f;
        
        double _double = 0.0;
        
        System.out.println("Byte : " + _byte + "\nShort : " + _short);
        System.out.println("Int : " + _int);
        System.out.println("Long : " + _long);
        System.out.println("Char : " + _char);
        System.out.println("Boolean : " + _boolean);
        System.out.println("Float : " + _float);
        System.out.println("Double : " + _double);
        
        System.out.println("Hello World");
        System.out.println("Hello\tWorld");
        System.out.println("Hello World\b\b");
        System.out.println("\"Hello World\"");




        
        
        
        
    }
    
}
