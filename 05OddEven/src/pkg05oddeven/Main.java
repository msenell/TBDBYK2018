package pkg05oddeven;

import java.util.Scanner;

/*
    Kullanıcıdan sayı al.
    Sayının tek mi çift mi olduğunu ekrana yaz.
*/
public class Main {

    public static void main(String[] args) 
    {
        int sayi = 0;
        Scanner scn = new Scanner(System.in);
        System.out.println("Bir sayi giriniz:");
        sayi = scn.nextInt();
        
        System.out.println("if");
        if(sayi%2 == 0)
            System.out.println("Girdiğiniz sayı çifttir.");
        else
            System.out.println("Girdiğiniz sayı tektir.");
        System.out.println("--------------------");
        System.out.println("switch");
        switch(sayi%2)
        {
            case 0: //Sayı çift ise:
                System.out.println("Sayı çifttir.");
                break;
            default: //Sayı çift değil ise:
                System.out.println("Sayı tektir.");
        }
        System.out.println("--------------------");
        String sonuc = sayi%2 == 0 ? "Çift" : "Tek";
        System.out.println("Kısa if tanımı:");
        System.out.println("Girilen sayı " + sonuc);
    }
    
    
    
}
