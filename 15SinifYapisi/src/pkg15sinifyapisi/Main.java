package pkg15sinifyapisi;

public class Main 
{
    public static void main(String[] args) 
    {
        Person p = new Person("Berkay", "Aydemir");
        p.setIsim("AhMeT");
        System.out.println("Main.java : ");
        System.out.println(p.getIsim() + " " + p.getSoyisim());
        p.setdYili(1995);
        System.out.println(p.getIsim() + " " + p.yasHesapla() + " yaşındadır.");
    }
    
}
