package pkg15sinifyapisi;

public class Person 
{
    private String isim;
    private String soyisim;
    private int dYili;
    static final String tür = "Homo Saphiens";
    //Method Overloading
    Person()
    { 
        System.out.println("Kişi oluştu");
    }
    Person(String ad)
    {
        isim = ad;
        System.out.println(isim + " oluştu");
    }
    Person(String isim, String soyisim)
    {
        this(isim);
        this.isim = isim;
        this.soyisim = soyisim;
        System.out.println("Person.java : ");
        System.out.println(isim + " " + this.soyisim + " oluştu");
    }
    
    public String getIsim()
    {
        return this.isim;
    }
    
    public String getSoyisim()
    {
        return this.soyisim;
    }
    
    public void setIsim(String isim)
    {
        if(isim.toUpperCase().equals("AHMET"))
            this.isim = "Şukufe";
        else
            this.isim = isim;
    }
    
    public void setSoyisim(String soyisim)
    {
        this.soyisim = soyisim;
    }

    public int getdYili() 
    {
        return dYili;
    }

    public void setdYili(int dYili) 
    {
        this.dYili = dYili;
    }      
    
    protected int yasHesapla()
    {
        if(this.dYili > 0)
            return 2018 - this.dYili;
        else
            return 0;
    }
}
