package pkg11wrappers;


public class Main 
{

    
    public static void main(String[] args) 
    {
        String s = "127";
        int a = 23;
        int b = 7;
        Integer nesneTamsayi = new Integer(Integer.parseInt(s));
        
        nesneTamsayi = Integer.valueOf(s);
        
        System.out.println(nesneTamsayi.intValue() + a);
        
        System.out.println(Integer.sum(a, b));
        
        System.out.println("Integer maximum değeri : " + Integer.MAX_VALUE);
        System.out.println("Integer minimum değeri : " + Integer.MIN_VALUE);
        System.out.println("Integer boyutu : " + Integer.SIZE + "bit");
        
        System.out.println("------------------");
        
        System.out.println(Character.compare('A', 'C'));
        System.out.println(Character.isDigit('5'));
        System.out.println(Character.isDigit('I'));
        System.out.println(Character.isLetter('G'));
        System.out.println(Character.isAlphabetic(64));
        System.out.println(Character.isLowerCase('C'));
        System.out.println(Character.isSpaceChar(' '));
        System.out.println(Character.isWhitespace('\b'));
        
        System.out.println("-------------------------");
        
        char[] isim = {'A', 'l', 'i', 'V', 'e', 'l', 'i', 'G', 'o', 'n', 'y', 'a'};
        
        String adi = String.copyValueOf(isim);
        //System.out.println(adi);
        
        adi = String.valueOf(isim, 0, 3);
        String soyadi = String.valueOf(isim, 3, 4);
        String memleket = String.valueOf(isim, 7, 5);
        System.out.println("İsim : " + adi);
        System.out.println("Soyisim : " + soyadi);
        System.out.println("Memleket : " + memleket);
        
        System.out.println();
        char[] sehir = new char[memleket.length()];
        memleket.getChars(0, memleket.length(), sehir, 0);
        System.out.println(sehir.length);
        

        





    }
    
}
