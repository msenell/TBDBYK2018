package pkg03modifiers;

//Java Modifiers

//Main sınıfı
public class Main 
{
    //Main sınıfına ait default erişim sevinde sayi değişkeni
    int sayi = 5;
    private int sayi2 = 3;

    public static void main(String[] args) 
    {
        //Main içindeki sayi değişkenine A sınıfı ile erişme
        A kisi1 = new A();
        A kisi2 = new A();
        
        kisi2.soyisim = "Mete";
        
        System.out.println("Kişi 1 : " + A.isim + " " + kisi1.soyisim);
        System.out.println("Kişi 2 : " + A.isim + " " + kisi2.soyisim);
        A.isim = "Ahmet";
        kisi1.soyisim = "Mavi";
        System.out.println("Kişi 1 : " + A.isim + " " + kisi1.soyisim);
        System.out.println("Kişi 2 : " + A.isim + " " + kisi2.soyisim);
        
        A.isimYaz();
        
        System.out.println(B.yas);
    }
    
}
